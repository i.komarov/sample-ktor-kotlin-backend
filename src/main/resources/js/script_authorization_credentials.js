/**
 * Created by i_komarov on 03.07.17.
 */
function sendCredentials(login, password) {
    var xhr = new XMLHttpRequest();
    var url = "http://localhost:5000/api/authorization";
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-type", "application/json");
    var data = JSON.stringify({"login": login, "password": password});
    xhr.send(data);
}

