package net.styleru.ikomarov.diabetics.mapping.dto

import com.google.gson.*
import org.joda.time.DateTime
import net.styleru.ikomarov.diabetics.extentions.dateTimeSerialize
import java.lang.reflect.Type

/**
 * Created by i_komarov on 28.06.17.
 */

class DateTimeSerializer: JsonSerializer<DateTime> {

    override fun serialize(src: DateTime?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement = when {
        src != null -> JsonPrimitive(dateTimeSerialize(src))
        else -> JsonNull.INSTANCE
    }
}