package net.styleru.ikomarov.diabetics.mapping.topics

import net.styleru.ikomarov.diabetics.dbo.topics.TopicDBO
import net.styleru.ikomarov.diabetics.dto.topics.TopicDTO
import net.styleru.ikomarov.diabetics.entities.topics.TopicEntity
import net.styleru.ikomarov.diabetics.extentions.dateTimeDeserializeSql
import net.styleru.ikomarov.diabetics.extentions.dateTimeSerializeSql

/**
 * Created by i_komarov on 02.07.17.
 */

fun TopicDBO.string(): String = "{" +
        "id:${id}," +
        "createdTime:${createdTime}," +
        "lastModifiedTime:${lastModifiedTime}," +
        "title:${title}" +
        "content:${content}" +
        "category:${category}," +
        "}"

fun TopicEntity.string(): String = "{" +
        "id:${id}," +
        "createdTime:${createdTime}," +
        "lastModifiedTime:${lastModifiedTime}," +
        "title:${title}" +
        "content:${content}" +
        "categoryId:${categoryId}" +
        "}"

fun TopicEntity.toDTO(): TopicDTO = TopicDTO(
        id = id!!,
        createdTime = createdTime!!,
        lastModifiedTime = lastModifiedTime!!,
        categoryId = categoryId,
        title = title,
        content = content,
        commentsCount = commentsCount
)

fun TopicEntity.toDBO(): TopicDBO = TopicDBO(
        id = id,
        createdTime = createdTime?.let { dateTimeSerializeSql(createdTime) },
        lastModifiedTime = lastModifiedTime?.let { dateTimeSerializeSql(lastModifiedTime) },
        categoryId = categoryId,
        title = title,
        content = content
)

fun TopicDBO.toEntity(): TopicEntity {
    val created = this.createdTime
    val lastModified = this.lastModifiedTime
    return TopicEntity(
            id = id,
            createdTime = created?.let { dateTimeDeserializeSql(created) },
            lastModifiedTime = lastModified?.let { dateTimeDeserializeSql(lastModified) },
            categoryId = category.id,
            title = title,
            content = content,
            //TODO("Add support for comments relationship")
            commentsCount = 0
    )
}