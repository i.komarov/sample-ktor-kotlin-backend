package net.styleru.ikomarov.diabetics.mapping.categories

import net.styleru.ikomarov.diabetics.dbo.cateogories.CategoryDBO
import net.styleru.ikomarov.diabetics.dto.categories.CategoryDTO
import net.styleru.ikomarov.diabetics.entities.categories.CategoryEntity
import net.styleru.ikomarov.diabetics.extentions.dateTimeDeserializeSql
import net.styleru.ikomarov.diabetics.extentions.dateTimeSerializeSql
import java.sql.Date

/**
 * Created by i_komarov on 01.07.17.
 */

fun CategoryDBO.string(): String = "{" +
        "id:$id," +
        "createdTime:$createdTime," +
        "lastModifiedTime:$lastModifiedTime," +
        "title:$title" +
        "}"

fun CategoryEntity.string(): String = "{" +
        "id:${id}," +
        "createdTime:${createdTime}," +
        "lastModifiedTime:${lastModifiedTime}," +
        "title:${title}" +
        "}"

fun CategoryDTO.toEntity(): CategoryEntity = CategoryEntity(
        title = title
)

fun CategoryEntity.toDTO(): CategoryDTO = CategoryDTO(
        id = id!!,
        createdTime = createdTime!!,
        lastModifiedTime = lastModifiedTime!!,
        title = title,
        topicsCount = topicsCount
)

fun CategoryEntity.toDBO(): CategoryDBO = CategoryDBO(
        id = id,
        createdTime = createdTime?.let { dateTimeSerializeSql(createdTime) },
        lastModifiedTime = lastModifiedTime?.let { dateTimeSerializeSql(lastModifiedTime) },
        title = title
)

fun CategoryDBO.toEntity(): CategoryEntity {
    val created = this.createdTime
    val lastModified = this.lastModifiedTime
    return CategoryEntity(
            id = id,
            createdTime = created?.let { dateTimeDeserializeSql(created) },
            lastModifiedTime = lastModified?.let { dateTimeDeserializeSql(lastModified) },
            title = title,
            topicsCount = topics.size
    )
}