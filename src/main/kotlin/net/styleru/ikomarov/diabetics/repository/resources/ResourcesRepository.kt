package net.styleru.ikomarov.diabetics.repository.resources

import java.io.File

/**
 * Created by i_komarov on 04.07.17.
 */
class ResourcesRepository(private val classLoader: ClassLoader = Thread.currentThread().contextClassLoader): IResourcesRepository {

    override fun find(path: String): File = File(
            classLoader.getResource(path).file
    )
}