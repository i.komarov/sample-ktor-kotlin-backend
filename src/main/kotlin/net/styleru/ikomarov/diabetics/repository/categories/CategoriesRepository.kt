package net.styleru.ikomarov.diabetics.repository.categories

import net.styleru.ikomarov.diabetics.dao.base.ICrudDAO
import net.styleru.ikomarov.diabetics.dbo.cateogories.CategoryDBO
import net.styleru.ikomarov.diabetics.entities.categories.CategoryEntity
import net.styleru.ikomarov.diabetics.exceptions.RecordNotExistsException
import net.styleru.ikomarov.diabetics.mapping.categories.toDBO
import net.styleru.ikomarov.diabetics.mapping.categories.toEntity
import org.joda.time.DateTime
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by i_komarov on 01.07.17.
 */
class CategoriesRepository constructor(
        private val logger: Logger = LoggerFactory.getLogger("CategoriesRepository"),
        private val database: ICrudDAO<CategoryDBO>
): ICategoriesRepository {

    override fun create(entity: CategoryEntity): CategoryEntity = with(entity.toDBO()) {
        database.create(this)
        toEntity()
    }

    override fun read(id: Long): CategoryEntity = database
            .read(id = id)
            .toEntity()

    override fun read(): List<CategoryEntity>  = database
            .readList()
            .map { it.toEntity() }

    override fun read(lastModified: DateTime): List<CategoryEntity> = database
            .readList(lastModified = lastModified)
            .map { it.toEntity() }

    override fun read(offset: Int, limit: Int): List<CategoryEntity> = database
            .readList(offset = offset, limit = limit)
            .map { it.toEntity() }

    override fun update(entity: CategoryEntity): CategoryEntity = with(entity.toDBO()) {
        database.update(this)
        database.read(id!!).toEntity()
    }

    override fun delete(id: Long): Long = database
            .delete(id)
            .let { id }
}