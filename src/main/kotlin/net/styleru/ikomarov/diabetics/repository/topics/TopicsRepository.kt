package net.styleru.ikomarov.diabetics.repository.topics

import net.styleru.ikomarov.diabetics.entities.topics.TopicEntity

import net.styleru.ikomarov.diabetics.dao.base.ICrudDAO
import net.styleru.ikomarov.diabetics.dbo.topics.TopicDBO
import net.styleru.ikomarov.diabetics.mapping.topics.toDBO
import net.styleru.ikomarov.diabetics.mapping.topics.toEntity

import org.joda.time.DateTime
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by i_komarov on 02.07.17.
 */
class TopicsRepository constructor(
        private val logger: Logger = LoggerFactory.getLogger("TopicsRepository"),
        private val database: ICrudDAO<TopicDBO>
): ITopicsRepository {

    override fun create(entity: TopicEntity): TopicEntity = with(entity.toDBO()) {
        database.create(this)
        toEntity()
    }

    override fun read(id: Long): TopicEntity = database.read(id)
            .toEntity()

    override fun readList(categoryId: Long): List<TopicEntity> = database.readList(categoryId)
            .map { it.toEntity() }

    override fun readList(categoryId: Long, lastModified: DateTime): List<TopicEntity> = database.readList(categoryId, lastModified)
            .map { it.toEntity() }

    override fun readList(categoryId: Long, offset: Int, limit: Int): List<TopicEntity> = database.readList(categoryId, offset, limit)
            .map { it.toEntity() }

    override fun update(entity: TopicEntity): TopicEntity = with(entity.toDBO()) {
        database.update(this)
        toEntity()
    }

    override fun delete(id: Long): Long = database.delete(id)
            .let { id }
}