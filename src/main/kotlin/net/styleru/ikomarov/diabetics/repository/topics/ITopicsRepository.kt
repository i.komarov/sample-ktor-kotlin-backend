package net.styleru.ikomarov.diabetics.repository.topics

import net.styleru.ikomarov.diabetics.entities.topics.TopicEntity
import org.joda.time.DateTime

/**
 * Created by i_komarov on 02.07.17.
 */
interface ITopicsRepository {

    fun create(entity: TopicEntity): TopicEntity

    fun read(id: Long): TopicEntity

    fun readList(categoryId: Long): List<TopicEntity>

    fun readList(categoryId: Long, lastModified: DateTime): List<TopicEntity>

    fun readList(categoryId: Long, offset: Int, limit: Int): List<TopicEntity>

    fun update(entity: TopicEntity): TopicEntity

    fun delete(id: Long): Long
}