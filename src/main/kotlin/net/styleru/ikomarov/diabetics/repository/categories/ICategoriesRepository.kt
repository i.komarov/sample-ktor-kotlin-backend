package net.styleru.ikomarov.diabetics.repository.categories

import net.styleru.ikomarov.diabetics.entities.categories.CategoryEntity
import org.joda.time.DateTime

/**
 * Created by i_komarov on 01.07.17.
 */
interface ICategoriesRepository {

    fun create(entity: CategoryEntity): CategoryEntity

    fun read(id: Long): CategoryEntity

    fun read(): List<CategoryEntity>

    fun read(lastModified: DateTime): List<CategoryEntity>

    fun read(offset: Int, limit: Int): List<CategoryEntity>

    fun update(entity: CategoryEntity): CategoryEntity

    fun delete(id: Long): Long
}