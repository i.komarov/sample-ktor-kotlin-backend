package net.styleru.ikomarov.diabetics.repository.resources

import java.io.File

/**
 * Created by i_komarov on 04.07.17.
 */
interface IResourcesRepository {

    fun find(path: String): File
}