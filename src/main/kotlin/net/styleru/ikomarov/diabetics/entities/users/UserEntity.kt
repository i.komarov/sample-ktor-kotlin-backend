package net.styleru.ikomarov.diabetics.entities.users

import net.styleru.ikomarov.diabetics.contracts.users.UserRole
import net.styleru.ikomarov.diabetics.entities.base.StandardEntity
import org.joda.time.DateTime

/**
 * Created by i_komarov on 03.07.17.
 */
class UserEntity(
        id: Long?,
        createdTime: DateTime?,
        lastModifiedTime: DateTime?,
        val firstName: String,
        val lastName: String,
        val imageUrl: String,
        val phone: String,
        val role: UserRole
): StandardEntity(id, createdTime, lastModifiedTime)