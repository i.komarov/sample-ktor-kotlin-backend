package net.styleru.ikomarov.diabetics.entities.topics

import net.styleru.ikomarov.diabetics.entities.base.StandardEntity
import org.joda.time.DateTime;

/**
 * Created by i_komarov on 02.07.17.
 */
class TopicEntity constructor(
        id: Long? = null,
        createdTime: DateTime? = null,
        lastModifiedTime: DateTime? = null,
        val categoryId: Long?,
        val title: String,
        val content: String,
        val commentsCount: Int = 0
): StandardEntity(id, createdTime, lastModifiedTime)