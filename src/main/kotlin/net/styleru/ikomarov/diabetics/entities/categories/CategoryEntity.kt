package net.styleru.ikomarov.diabetics.entities.categories

import org.joda.time.DateTime
import net.styleru.ikomarov.diabetics.entities.base.StandardEntity

/**
 * Created by i_komarov on 28.06.17.
 */
class CategoryEntity constructor(
        id: Long? = null,
        createdTime: DateTime? = null,
        lastModifiedTime: DateTime? = null,
        val title: String,
        val topicsCount: Int = 0
) : StandardEntity(id, createdTime, lastModifiedTime)