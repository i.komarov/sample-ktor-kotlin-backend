package net.styleru.ikomarov.diabetics.entities.base

import org.joda.time.DateTime
import net.styleru.ikomarov.diabetics.extentions.currentDateTime

/**
 * Created by i_komarov on 28.06.17.
 */
open class StandardEntity protected constructor(
        val id: Long? = null,
        val createdTime: DateTime? = null,
        val lastModifiedTime: DateTime? = null
)