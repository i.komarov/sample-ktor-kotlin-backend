package net.styleru.ikomarov.diabetics.controllers.categories

import com.google.gson.Gson
import net.styleru.ikomarov.diabetics.controllers.errors.HttpErrorProcessor
import net.styleru.ikomarov.diabetics.dto.categories.NullableCategoryDTO
import net.styleru.ikomarov.diabetics.dto.response.StandardResponse
import net.styleru.ikomarov.diabetics.entities.categories.CategoryEntity
import net.styleru.ikomarov.diabetics.exceptions.RecordNotExistsException
import net.styleru.ikomarov.diabetics.extentions.dateTimeDeserialize
import net.styleru.ikomarov.diabetics.mapping.categories.toDTO
import net.styleru.ikomarov.diabetics.repository.categories.ICategoriesRepository
import org.jetbrains.ktor.application.ApplicationCall
import org.jetbrains.ktor.application.call
import org.jetbrains.ktor.application.receive
import org.jetbrains.ktor.http.ContentType
import org.jetbrains.ktor.http.HttpStatusCode
import org.jetbrains.ktor.http.parseUrlEncodedParameters
import org.jetbrains.ktor.response.respondText
import org.jetbrains.ktor.routing.Route
import org.jetbrains.ktor.routing.get
import org.jetbrains.ktor.routing.post
import org.jetbrains.ktor.routing.put
import org.jetbrains.ktor.request.*
import org.joda.time.DateTime
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.nio.charset.Charset

/**
 * Created by i_komarov on 28.06.17.
 */

/**
 * This function intercepts incoming requests and declares general structure of /api/categories endpoint requests
 * @param logger logger for debug purposes
 * @param repository repository pattern implementation responsible for performing operations on category entities
 * @param gson json serializer/deserializer
 * @param errorProcessor component responsible for responding in exceptional cases
 * */
fun Route.categoriesApi(logger: Logger = LoggerFactory.getLogger("CategoriesController"), gson: Gson, repository: ICategoriesRepository, errorProcessor: HttpErrorProcessor) {
    get("/api/categories/{id?}") {
        try {
            dispatchCategoriesGetRequest(
                    call = call,
                    repository = repository,
                    gson = gson,
                    errorProcessor = errorProcessor
            )
        } catch(e: Exception) {
            logger.debug("Unexpected exception", e)
            errorProcessor.processError(
                    call,
                    HttpStatusCode.InternalServerError,
                    "Error occured on server-side: ${e.message}"
            )
        }
    }

    post("/api/categories/{id?}") {
        try {
            dispatchCategoriesPostRequest(
                    call = call,
                    repository = repository,
                    gson = gson,
                    errorProcessor = errorProcessor
            )
        } catch(e: Exception) {
            logger.debug("Unexpected exception", e)
            errorProcessor.processError(
                    call,
                    HttpStatusCode.InternalServerError,
                    "Error occured on server-side: ${e.message}"
            )
        }
    }

    put("/api/categories/{id?}") {
        try {
            dispatchCategoriesPutRequest(
                    call = call,
                    repository = repository,
                    gson = gson,
                    errorProcessor = errorProcessor
            )
        } catch(e: Exception) {
            logger.debug("Unexpected exception", e)
            errorProcessor.processError(
                    call,
                    HttpStatusCode.InternalServerError,
                    "error occured on server-side: ${e.message}"
            )
        }
    }
}
/**
 * This function performs dispatching of incoming get requests to /api/categories endpoint depending on params passed
 * Routes call to one of next handler-functions:
 * 1) function that retrieves category with particular id
 * 2) function that retrieves list of categories for "offset" position of size "limit"
 * 3) function that retrieves list of categories created or modified since the date passed as "lastModified"
 * @param call incoming http call controller
 * @param repository repository pattern implementation responsible for performing operations on category entities
 * @param gson json serializer/deserializer
 * @param errorProcessor component responsible for responding in exceptional cases
 * */
suspend fun dispatchCategoriesGetRequest(call: ApplicationCall, repository: ICategoriesRepository, gson: Gson, errorProcessor: HttpErrorProcessor) = when {
    call.parameters.contains("limit") -> {
        with(call.parameters) {
            val offset = this["offset"]?.toIntOrNull()
            val limit = this["limit"]?.toIntOrNull()
            if(limit != null) {
                handleCategoriesGet(
                        call = call,
                        repository = repository,
                        gson = gson,
                        errorProcessor = errorProcessor,
                        offset = offset ?: 0,
                        limit = limit
                )
            }
        }
    }
    call.parameters.contains("last_modified") -> {
        with(call.parameters) {
            if(this["last_modified"] != null) {
                val lastModified = this["last_modified"]
                if (lastModified != null) {
                    handleCategoriesGet(
                            call = call,
                            repository = repository,
                            gson = gson,
                            errorProcessor = errorProcessor,
                            lastModified = dateTimeDeserialize(lastModified)
                    )
                }
            }
        }
    }
    call.request.headers.contains("if-modified-since") || call.request.headers.contains("IF-MODIFIED-SINCE") -> {
        with(call.request.headers) {
            val lastModified = this["if-modified-since"] ?: this["IF-MODIFIED-SINCE"]
            if(lastModified != null) {
                handleCategoriesGet(
                        call = call,
                        repository = repository,
                        gson = gson,
                        errorProcessor = errorProcessor,
                        lastModified = dateTimeDeserialize(lastModified)
                )
            }
        }
    }
    call.parameters.contains("id") -> {
        with(call.parameters) {
            val id = this["id"]?.toLongOrNull()
            if(id != null) {
                handleCategoriesGet(
                        call = call,
                        repository = repository,
                        gson = gson,
                        errorProcessor = errorProcessor,
                        id = id
                )
            }
        }
    }
    else -> errorProcessor.processError(
            call,
            HttpStatusCode.BadRequest,
            "Not all of required parameters were set"
    )
}
/**
* This function handles the case when category matching the particular id is needed
* @param call incoming http call controller
* @param repository repository pattern implementation responsible for performing operations on category entities
* @param gson json serializer/deserializer
* @param errorProcessor component responsible for responding in exceptional cases
* @param id identifier of category needed
* */
suspend fun handleCategoriesGet(call: ApplicationCall, repository: ICategoriesRepository, gson: Gson, errorProcessor: HttpErrorProcessor, id: Long) = with(call) {
    try {
        response.status(HttpStatusCode.OK)
        respondText(
                gson.toJson(StandardResponse.success(repository.read(id).toDTO())),
                ContentType.Application.Json
        )
    } catch(e: RecordNotExistsException) {
        errorProcessor.processError(
                call,
                HttpStatusCode.BadRequest,
                e.localizedMessage
        )
    }
}
/**
 * This function handles the case when a list of categories created/modified since "lastModified" value is needed
 * @param call incoming http call controller
 * @param repository repository pattern implementation responsible for performing operations on category entities
 * @param gson json serializer/deserializer
 * @param errorProcessor component responsible for responding in exceptional cases
 * @param lastModified the date since which the categories are returned
 * */
suspend fun handleCategoriesGet(call: ApplicationCall, repository: ICategoriesRepository, gson: Gson, errorProcessor: HttpErrorProcessor, lastModified: DateTime) = with(call) {
    response.status(HttpStatusCode.OK)
    respondText(
            gson.toJson(StandardResponse.success(repository.read(lastModified).map { it.toDTO() })),
            ContentType.Application.Json
    )
}
/**
 * This function handles the case when a list of categories since the "offset" of size "limit" is needed
 * @param call incoming http call controller
 * @param repository repository pattern implementation responsible for performing operations on category entities
 * @param gson json serializer/deserializer
 * @param errorProcessor component responsible for responding in exceptional cases
 * @param offset the category position in the database that will be the first to appear in response list
 * @param limit the size of response list
 * */
suspend fun handleCategoriesGet(call: ApplicationCall, repository: ICategoriesRepository, gson: Gson, errorProcessor: HttpErrorProcessor, offset: Int = 0, limit: Int) = with(call) {
    response.status(HttpStatusCode.OK)
    respondText(
            gson.toJson(StandardResponse.success(repository.read(offset, limit).map { it.toDTO() })),
            ContentType.Application.Json
    )
}
/**
 * This function performs dispatching of incoming post requests to /categories endpoint depending on params passed
 * Routes call to one of next handler-functions:
 * 1) function that creates new category
 * @param call incoming http call controller
 * @param repository repository pattern implementation responsible for performing operations on category entities
 * @param gson json serializer/deserializer
 * @param errorProcessor component responsible for responding in exceptional cases
 * */
suspend fun dispatchCategoriesPostRequest(call: ApplicationCall, repository: ICategoriesRepository, gson: Gson, errorProcessor: HttpErrorProcessor)  = with(call) {
    when {
        request.contentType() == ContentType.Application.Json -> handleCategoriesPost(
                call = call,
                repository = repository,
                gson = gson,
                errorProcessor = errorProcessor,
                dto = gson.fromJson(request.receive<String>(), NullableCategoryDTO::class.java)
        )
        else -> errorProcessor.processError(
                call,
                HttpStatusCode.BadRequest,
                "Content-Type ${call.request.contentType()} is not supported for this request"
        )
    }
}
/**
 * This function handles the case when new category is needed to be created
 * @param call incoming http call controller
 * @param repository repository pattern implementation responsible for performing operations on category entities
 * @param gson json serializer/deserializer
 * @param errorProcessor component responsible for responding in exceptional cases
 * @param dto object representing the json passed that contains data to create new category
 * */
suspend fun handleCategoriesPost(call: ApplicationCall, repository: ICategoriesRepository, gson: Gson, errorProcessor: HttpErrorProcessor, dto: NullableCategoryDTO) = with(call) {
    when {
        dto.id == null && dto.topicsCount == null && dto.createdTime == null && dto.lastModifiedTime == null && dto.title != null -> {
            val createdDTO = repository.create(CategoryEntity(title = dto.title)).toDTO()
            call.response.status(
                    when {
                        createdDTO.createdTime == createdDTO.lastModifiedTime -> HttpStatusCode.Created
                        else -> HttpStatusCode.OK
                    }
            )
            respondText(
                    gson.toJson(StandardResponse.success(createdDTO)),
                    ContentType.Application.Json
            )
        }
        dto.title == null -> errorProcessor.processError(
                call,
                HttpStatusCode.BadRequest,
                "Required property \"title\" was not set"
        )
        else -> {
            var message = "Following properties are not supported: "
            if(dto.id != null) {
                message = message.plus("\"id\",")
            }
            if(dto.createdTime != null) {
                message = message.plus("\"created_time\",")
            }
            if(dto.lastModifiedTime != null) {
                message = message.plus("\"last_modified_time\",")
            }
            if(dto.topicsCount != null) {
                message = message.plus("\"topics_count\",")
            }

            message = message.substringBeforeLast(',')
            errorProcessor.processError(
                    call,
                    HttpStatusCode.BadRequest,
                    message
            )
        }
    }
}
/**
 * This function performs dispatching of incoming put requests to /categories endpoint depending on params passed
 * Routes call to one of next handler-functions:
 * 1) function that updates existing or creates new category
 * @param call incoming http call controller
 * @param repository repository pattern implementation responsible for performing operations on category entities
 * @param gson json serializer/deserializer
 * @param errorProcessor component responsible for responding in exceptional cases
 * */
suspend fun dispatchCategoriesPutRequest(call: ApplicationCall, repository: ICategoriesRepository, gson: Gson, errorProcessor: HttpErrorProcessor) = with(call.parameters["id"]) {
    when {
        this != null && this.toLongOrNull() != null -> {
            handleCategoriesPut(
                    call = call,
                    repository = repository,
                    gson = gson,
                    errorProcessor = errorProcessor,
                    id = this.toLong()
            )
        }
        this != null && this.toLongOrNull() == null ->  errorProcessor.processError(
                call,
                HttpStatusCode.BadRequest,
                "Path part \"id\" is of illegal value type, long required"
        )
        else -> errorProcessor.processError(
                call,
                HttpStatusCode.BadRequest,
                "Required path part \"id\" was not set"
        )
    }
}
/**
 * This function handles the case when existing category needs to be updated
 * @param call incoming http call controller
 * @param repository repository pattern implementation responsible for performing operations on category entities
 * @param gson json serializer/deserializer
 * @param errorProcessor component responsible for responding in exceptional cases
 * @param id identifier of category to be updated
 * */
suspend fun handleCategoriesPut(call: ApplicationCall, repository: ICategoriesRepository, gson: Gson, errorProcessor: HttpErrorProcessor, id: Long) = with(call.request) {
    when {
        contentType() == ContentType.Application.FormUrlEncoded -> {
            val params = receive(String::class).parseUrlEncodedParameters(contentCharset() ?: Charsets.UTF_8)
            val title = params["title"]
            when {
                title != null -> {
                    val updatedDTO = repository.update(CategoryEntity(id = id, title = title)).toDTO()
                    call.response.status(
                            when {
                                updatedDTO.createdTime == updatedDTO.lastModifiedTime -> HttpStatusCode.Created
                                else -> HttpStatusCode.OK
                            }
                    )
                    call.respondText(
                            gson.toJson(StandardResponse.success(updatedDTO)),
                            ContentType.Application.Json
                    )
                }
                else -> errorProcessor.processError(
                        call,
                        HttpStatusCode.BadRequest,
                        "Not all of params required were set"
                )
            }
        }
        else -> errorProcessor.processError(
                call,
                HttpStatusCode.BadRequest,
                "Content-Type ${call.request.contentType()} is not supported for this request"
        )
    }
}

@Deprecated(message = "example of deserialization for incoming multipart/form-data request", level = DeprecationLevel.HIDDEN)
suspend fun handleMultipartRequest(call: ApplicationCall, logger: Logger? = null) = when {
    call.request.isMultipart() -> {
        call.request.receive(MultiPartData::class).parts.forEach { part ->
            when(part) {
                is PartData.FormItem -> {
                    logger?.debug("multipart[${part.partName}:${part.value}]")
                }
                is PartData.FileItem -> {
                    logger?.debug("multipart[${part.partName}:${part.originalFileName} of ${part.contentType}]")
                }
            }

            part.dispose()
        }
    }
    else -> {

    }
}

