package net.styleru.ikomarov.diabetics.controllers.errors

import com.google.gson.Gson
import net.styleru.ikomarov.diabetics.dto.error.ErrorDTO
import net.styleru.ikomarov.diabetics.dto.response.StandardResponse
import org.jetbrains.ktor.application.ApplicationCall
import org.jetbrains.ktor.http.ContentType
import org.jetbrains.ktor.http.HttpStatusCode
import org.jetbrains.ktor.response.respondText

/**
 * Created by i_komarov on 03.07.17.
 */
class HttpErrorProcessor private constructor(private val gson: Gson, private val endpoint: String) {

    class Factory(private val gson: Gson) {

        fun create(endpoint: String): HttpErrorProcessor = HttpErrorProcessor(
                gson = gson,
                endpoint = endpoint
        )
    }

    suspend fun processError(call: ApplicationCall, status: HttpStatusCode, message: String) = with(call) {
        response.status(status)
        respondText(
                text = gson.toJson(StandardResponse.failure<Any?>(ErrorDTO(message = message, apiUrl = endpoint))),
                contentType = ContentType.Application.Json
        )
    }
}