package net.styleru.ikomarov.diabetics.controllers.authorization

import com.google.gson.Gson
import net.styleru.ikomarov.diabetics.controllers.errors.HttpErrorProcessor
import net.styleru.ikomarov.diabetics.repository.resources.ResourcesRepository
import org.jetbrains.ktor.application.ApplicationCall
import org.jetbrains.ktor.application.call
import org.jetbrains.ktor.content.LocalFileContent
import org.jetbrains.ktor.http.ContentType
import org.jetbrains.ktor.http.HttpStatusCode
import org.jetbrains.ktor.response.contentType
import org.jetbrains.ktor.routing.Route
import org.jetbrains.ktor.routing.get
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by i_komarov on 03.07.17.
 */

fun Route.authorizationApiScripts(repository: ResourcesRepository, logger: Logger = LoggerFactory.getLogger("AuthorizationScriptsController"), gson: Gson, errorProcessor: HttpErrorProcessor) {
    get("/api/authorization/scripts/{name}") {
        try {
            dispatchAuthorizationScriptRequest(
                    call = call,
                    gson = gson,
                    repository = repository,
                    errorProcessor = errorProcessor
            )
        } catch(e: Exception) {
            logger.debug("Unexpected exception", e)
            errorProcessor.processError(
                    call,
                    HttpStatusCode.InternalServerError,
                    "Error occured on server-side: ${e.message}"
            )
        }
    }
}

suspend fun dispatchAuthorizationScriptRequest(logger: Logger? = null, call: ApplicationCall, repository: ResourcesRepository, gson: Gson, errorProcessor: HttpErrorProcessor) = with(call.parameters["name"]) {
    logger?.debug("incoming request for $this script dispatched")
    when {
        this == null -> errorProcessor.processError(
                call = call,
                status = HttpStatusCode.BadRequest,
                message = "Required path part \"name\" was not set"
        )
        else -> dispatchAuthorizationScriptRequest(
                call = call,
                gson = gson,
                repository = repository,
                errorProcessor = errorProcessor,
                name = this
        )
    }
}

suspend fun dispatchAuthorizationScriptRequest(call: ApplicationCall, gson: Gson, repository: ResourcesRepository, errorProcessor: HttpErrorProcessor, name: String) = when(name) {
    "script_authorization_credentials.js" -> with(call) {
        response.contentType(ContentType.Text.JavaScript)
        response.status(HttpStatusCode.OK)
        respond(LocalFileContent(repository.find("js/script_authorization_credentials.js")))
    }
    else -> errorProcessor.processError(
            call = call,
            status = HttpStatusCode.BadRequest,
            message = "Required path part \"name\" is of illegal type"
    )
}