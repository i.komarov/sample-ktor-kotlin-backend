package net.styleru.ikomarov.diabetics.controllers.users

import com.google.gson.Gson
import net.styleru.ikomarov.diabetics.controllers.errors.HttpErrorProcessor
import org.jetbrains.ktor.application.ApplicationCall
import org.jetbrains.ktor.application.call
import org.jetbrains.ktor.content.LocalFileContent
import org.jetbrains.ktor.http.ContentType
import org.jetbrains.ktor.http.HttpStatusCode
import org.jetbrains.ktor.response.contentType
import org.jetbrains.ktor.routing.Route
import org.jetbrains.ktor.routing.get
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import javax.imageio.ImageIO

/**
 * Created by i_komarov on 03.07.17.
 */

private object DefaultImage {
    val raw by lazy { File(Thread.currentThread().contextClassLoader.getResource("images/image_default.png").file) }
    val self by lazy { ImageIO.read(raw) }
    val width by lazy { self.width }
    val height by lazy { self.height }
}

/**
 * This function intercepts incoming requests and declares general structure of /api/users/image and /api/users/{id?}/image endpoints requests
 * @param logger logger for debug purposes
 * @param gson json serializer/deserializer
 * @param errorProcessor component responsible for responding in exceptional cases
 * */
fun Route.userImagesApi(logger: Logger = LoggerFactory.getLogger("UserImagesController"), gson: Gson, errorProcessor: HttpErrorProcessor) {
    get("/api/users/{id?}/image") {
        try {
            dispatchUserImageGetRequest(
                    logger = logger,
                    call = call,
                    gson = gson,
                    errorProcessor = errorProcessor
            )
        } catch(e: Exception) {
            logger.debug("Unexpected exception", e)
            errorProcessor.processError(
                    call,
                    HttpStatusCode.InternalServerError,
                    "Error occured on server-side: ${e.message}"
            )
        }
    }
    get("/api/users/image") {
        try {
            dispatchUserImageGetRequest(
                    logger = logger,
                    call = call,
                    gson = gson,
                    errorProcessor = errorProcessor
            )
        } catch(e: Exception) {
            logger.debug("Unexpected exception", e)
            errorProcessor.processError(
                    call,
                    HttpStatusCode.InternalServerError,
                    "Error occured on server-side: ${e.message}"
            )
        }
    }
}
/**
 * This function performs dispatching of incoming get requests to /users/{id?}/image and /users/image depending on params passed
 * Routes call to one of next handler-functions:
 * 1) function that retrieves the default profile image if no id was specified or image was not set for given identifier
 * 2) function that retrieves image matching given user identifier
 * @param call incoming http call controller
 * @param gson json serializer/deserializer
 * @param errorProcessor component responsible for responding in exceptional cases
 * */
suspend fun dispatchUserImageGetRequest(logger: Logger?, call: ApplicationCall, gson: Gson, errorProcessor: HttpErrorProcessor) = with(call.request.headers.getAll("Accept")) {
    logger?.debug("dispatchUserImageGetRequest;Content-Type: ${this}")
    when {
        this != null && (any { it.contains("*/*", true) || it.contains("image/*", true) || it.contains("image/png", true) }) -> {
            with(call.parameters["id"]) {
                when {
                    this == null -> handleUserImageDefaultGet(
                            logger = logger,
                            call = call,
                            gson = gson,
                            errorProcessor = errorProcessor
                    )
                    else -> handleUserImageCustomGet(
                            logger = logger,
                            call = call,
                            gson = gson,
                            errorProcessor = errorProcessor
                    )
                }
            }
        }
        else -> {
            errorProcessor.processError(
                    call,
                    HttpStatusCode.BadRequest,
                    "Accept header is not specified properly or its value is incorrect"
            )
        }
    }
}
/**
 * This function handles the case when user previously uploaded the image and specified the id in request
 * @param call incoming http call controller
 * @param gson json serializer/deserializer
 * @param errorProcessor component responsible for responding in exceptional cases
 * */
@Deprecated(message = "Unsupported operation", level = DeprecationLevel.WARNING)
suspend fun handleUserImageCustomGet(logger: Logger? = null, call: ApplicationCall, gson: Gson, errorProcessor: HttpErrorProcessor) = with(call) {
    //TODO("add support for custom user images")
    response.contentType(ContentType.Image.Any)
    errorProcessor.processError(
            call,
            HttpStatusCode.NotImplemented,
            "Custom user images feature is not supported yet"
    )
}
/**
 * This function handles the case when user either has not uploaded the image or specified id in request
 * Retrieves defaul profile image
 * @param call incoming http call controller
 * @param gson json serializer/deserializer
 * @param errorProcessor component responsible for responding in exceptional cases
 * */
suspend fun handleUserImageDefaultGet(logger: Logger? = null, call: ApplicationCall, gson: Gson, errorProcessor: HttpErrorProcessor) = with(call) {
    response.contentType(ContentType.Image.PNG)
    response.status(HttpStatusCode.OK)
    respond(LocalFileContent(DefaultImage.raw))
}