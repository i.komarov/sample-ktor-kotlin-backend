package net.styleru.ikomarov.diabetics.controllers.registration

import com.google.gson.Gson
import net.styleru.ikomarov.diabetics.controllers.errors.HttpErrorProcessor
import org.jetbrains.ktor.application.call
import org.jetbrains.ktor.http.HttpStatusCode
import org.jetbrains.ktor.routing.Route
import org.jetbrains.ktor.routing.post
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by i_komarov on 03.07.17.
 */

fun Route.registrationApi(logger: Logger = LoggerFactory.getLogger("RegistrationController"), gson: Gson, errorProcessor: HttpErrorProcessor) {
    post("/api/registration") {
        try {
            call.response.status(HttpStatusCode.NotImplemented)
            errorProcessor.processError(
                    call,
                    HttpStatusCode.NotImplemented,
                    "Registration feature is not supported yet"
            )
        } catch(e: Exception) {
            logger.debug("Unexpected exception", e)
            errorProcessor.processError(
                    call,
                    HttpStatusCode.InternalServerError,
                    "Error occured on server-side: ${e.message}"
            )
        }
    }
}