package net.styleru.ikomarov.diabetics.controllers.authorization

import com.google.gson.Gson
import net.styleru.ikomarov.diabetics.controllers.errors.HttpErrorProcessor
import net.styleru.ikomarov.diabetics.dto.authorization.CredentialsDTO
import org.jetbrains.ktor.application.ApplicationCall
import org.jetbrains.ktor.application.call
import org.jetbrains.ktor.http.HttpStatusCode
import org.jetbrains.ktor.routing.Route
import org.jetbrains.ktor.routing.post
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by i_komarov on 03.07.17.
 */

fun Route.authorizationApi(logger: Logger = LoggerFactory.getLogger("AuthorizationController"), gson: Gson, errorProcessor: HttpErrorProcessor) {
    post("/api/authorization") {
        try {
            logger.debug("Credentials data received, dispatching")
            dispatchAuthorizationPostRequest(
                    logger = logger,
                    call = call,
                    gson = gson,
                    errorProcessor = errorProcessor
            )
        } catch(e: Exception) {
            logger.debug("Unexpected exception", e)
            errorProcessor.processError(
                    call,
                    HttpStatusCode.InternalServerError,
                    "Error occured on server-side: ${e.message}"
            )
        }
    }
}

suspend fun dispatchAuthorizationPostRequest(logger: Logger? = null, call: ApplicationCall, gson: Gson, errorProcessor: HttpErrorProcessor) = with(call.request.headers.getAll("Content-Type")) {
    logger?.debug("Credentials data received, parsing")
    when {
        this != null -> when {
            any { contains("application/json") } -> {
                handleAuthorizationPostRequest(
                        logger = logger,
                        call = call,
                        gson = gson,
                        errorProcessor = errorProcessor
                )
            }
            else -> {
                errorProcessor.processError(
                        call = call,
                        status = HttpStatusCode.BadRequest,
                        message = "Content-Type is of illegal value"
                )
            }
        }
        else -> {
            errorProcessor.processError(
                    call = call,
                    status = HttpStatusCode.BadRequest,
                    message = "Content-Type must be set for this request"
            )
        }
    }
}

suspend fun handleAuthorizationPostRequest(logger: Logger? = null, call: ApplicationCall, gson: Gson, errorProcessor: HttpErrorProcessor) = with(call.request.receive(String::class)) {
    logger?.debug("Credentials received: ${this}")
    val credentials: CredentialsDTO = gson.fromJson(this, CredentialsDTO::class.java)
    logger?.debug("Credentials received: {\"login\":\"${credentials.login}\",\"password\":\"${credentials.password}\"}")
}