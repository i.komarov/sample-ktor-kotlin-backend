package net.styleru.ikomarov.diabetics.controllers.topics

import com.google.gson.Gson
import net.styleru.ikomarov.diabetics.controllers.errors.HttpErrorProcessor
import net.styleru.ikomarov.diabetics.dto.response.StandardResponse
import net.styleru.ikomarov.diabetics.exceptions.RecordNotExistsException
import net.styleru.ikomarov.diabetics.extentions.dateTimeDeserialize
import net.styleru.ikomarov.diabetics.mapping.topics.toDTO
import net.styleru.ikomarov.diabetics.repository.topics.ITopicsRepository
import org.jetbrains.ktor.application.ApplicationCall
import org.jetbrains.ktor.application.call
import org.jetbrains.ktor.http.ContentType
import org.jetbrains.ktor.http.HttpStatusCode
import org.jetbrains.ktor.response.respondText
import org.jetbrains.ktor.routing.Route
import org.jetbrains.ktor.routing.get
import org.joda.time.DateTime
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by i_komarov on 02.07.17.
 */

/**
 * This function intercepts incoming requests and declares general structure of /api/categories/{category_id}/topics/{id?} endpoint requests
 * @param logger logger for debug purposes
 * @param repository repository pattern implementation responsible for performing operations on topic entities
 * @param gson json serializer/deserializer
 * */
fun Route.topicsApi(logger: Logger = LoggerFactory.getLogger("TopicsController"), repository: ITopicsRepository, gson: Gson, errorProcessor: HttpErrorProcessor) {
    get("/api/categories/{category_id}/topics/{id?}") {
        try {
            dispatchTopicsGetRequest(
                    call = call,
                    repository = repository,
                    gson = gson,
                    errorProcessor = errorProcessor
            )
        } catch(e: Exception) {
            logger.debug("Unexpected exception", e)
            errorProcessor.processError(
                    call,
                    HttpStatusCode.InternalServerError,
                    "Error occured on server-side: ${e.message}"
            )
        }
    }
}
/**
 * This function performs pre-validation of incoming request, checking whether required url path part "category_id" was set
 * @param call incoming http call controller
 * @param repository repository pattern implementation responsible for performing operations on topic entities
 * @param gson json serializer/deserializer
 * */
suspend fun dispatchTopicsGetRequest(call: ApplicationCall, repository: ITopicsRepository, gson: Gson, errorProcessor: HttpErrorProcessor) = with(call.parameters["category_id"]) {
    when {
        this != null && this.toLongOrNull() != null -> {
            dispatchTopicsGetRequest(call, repository, gson, errorProcessor, this.toLong())
        }
        this != null && this.toLongOrNull() == null -> errorProcessor.processError(
                call,
                HttpStatusCode.BadRequest,
                "Path part \"category_id\" is of illegal value type, long required"
        )
        else -> errorProcessor.processError(
                call,
                HttpStatusCode.BadRequest,
                "Required path part \"category_id\" was not set"
        )
    }
}
/**
 * This function performs dispatching of incoming requests to /api/categories/{category_id}/topics/{id?} endpoint depending on params passed
 * Routes call to one of next handler-functions:
 * 1) function that retrieves topic with particular id
 * 2) function that retrieves list of topics for "offset" position of size "limit"
 * 3) function that retrieves list of topics created or modified since the date passed as "lastModified"
 * @param call incoming http call controller
 * @param repository repository pattern implementation responsible for performing operations on topic entities
 * @param gson json serializer/deserializer
 * @param categoryId unique identifier matching the category to search topics in
 * */
suspend fun dispatchTopicsGetRequest(call: ApplicationCall, repository: ITopicsRepository, gson: Gson, errorProcessor: HttpErrorProcessor, categoryId: Long) = when {
    call.parameters.contains("limit") -> {
        with(call.parameters) {
            val offset = this["offset"]?.toIntOrNull()
            val limit = this["limit"]?.toIntOrNull()
            if(limit != null) {
                handleTopicsGet(
                        call = call,
                        repository = repository,
                        gson = gson,
                        errorProcessor = errorProcessor,
                        categoryId = categoryId,
                        offset = offset ?: 0,
                        limit = limit
                )
            }
        }
    }
    call.parameters.contains("last_modified") -> {
        with(call.parameters) {
            if(this["last_modified"] != null) {
                val lastModified = this["last_modified"]
                if (lastModified != null) {
                    handleTopicsGet(
                            call = call,
                            repository = repository,
                            gson = gson,
                            errorProcessor = errorProcessor,
                            categoryId = categoryId,
                            lastModified = dateTimeDeserialize(lastModified)
                    )
                }
            }
        }
    }
    call.request.headers.contains("if-modified-since") || call.request.headers.contains("IF-MODIFIED-SINCE") -> {
        with(call.request.headers) {
            val lastModified = this["if-modified-since"] ?: this["IF-MODIFIED-SINCE"]
            if(lastModified != null) {
                handleTopicsGet(
                        call = call,
                        repository = repository,
                        gson = gson,
                        errorProcessor = errorProcessor,
                        categoryId = categoryId,
                        lastModified = dateTimeDeserialize(lastModified)
                )
            }
        }
    }
    call.parameters.contains("id") -> {
        with(call.parameters) {
            val id = this["id"]?.toLongOrNull()
            if(id != null) {
                handleTopicsGet(
                        call = call,
                        repository = repository,
                        gson = gson,
                        errorProcessor = errorProcessor,
                        id = id
                )
            }
        }
    }
    else -> errorProcessor.processError(
            call,
            HttpStatusCode.BadRequest,
            "Not all of params required were set"
    )
}
/**
 * This function handles the case when topic matching the particular id is needed
 * @param call incoming http call controller
 * @param repository repository pattern implementation responsible for performing operations on topic entities
 * @param gson json serializer/deserializer
 * @param id identifier of topic needed
 * */
suspend fun handleTopicsGet(call: ApplicationCall, repository: ITopicsRepository, gson: Gson, errorProcessor: HttpErrorProcessor, id: Long) = with(call) {
    try {
        response.status(HttpStatusCode.OK)
        respondText(
                gson.toJson(StandardResponse.success(repository.read(id).toDTO())),
                ContentType.Application.Json
        )
    } catch(e: RecordNotExistsException) {
        errorProcessor.processError(
                call,
                HttpStatusCode.BadRequest,
                e.localizedMessage
        )
    }
}
/**
 * This function handles the case when a list of topics created/modified since "lastModified" value is needed
 * @param call incoming http call controller
 * @param repository repository pattern implementation responsible for performing operations on topic entities
 * @param gson json serializer/deserializer
 * @param categoryId unique identifier matching the category to search topics in
 * */
suspend fun handleTopicsGet(call: ApplicationCall, repository: ITopicsRepository, gson: Gson, errorProcessor: HttpErrorProcessor, categoryId: Long, lastModified: DateTime) = with(call) {
    response.status(HttpStatusCode.OK)
    respondText(
            gson.toJson(StandardResponse.success(repository.readList(categoryId, lastModified).map { it.toDTO() })),
            ContentType.Application.Json
    )
}
/**
 * This function handles the case when a list of topics since the "offset" of size "limit" is needed
 * @param call incoming http call controller
 * @param repository repository pattern implementation responsible for performing operations on topic entities
 * @param gson json serializer/deserializer
 * @param categoryId unique identifier matching the category to search topics in
 * */
suspend fun handleTopicsGet(call: ApplicationCall, repository: ITopicsRepository, gson: Gson, errorProcessor: HttpErrorProcessor, categoryId: Long, offset: Int = 0, limit: Int) = with(call) {
    response.status(HttpStatusCode.OK)
    respondText(
            gson.toJson(StandardResponse.success(repository.readList(categoryId, offset, limit).map { it.toDTO() })),
            ContentType.Application.Json
    )
}
