package net.styleru.ikomarov.diabetics.extentions

import net.styleru.ikomarov.diabetics.extentions.DateTimeProvider.currentZone
import net.styleru.ikomarov.diabetics.extentions.DateTimeProvider.inputDateTimeFormatter
import net.styleru.ikomarov.diabetics.extentions.DateTimeProvider.outputDateTimeFormatter
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import java.sql.Timestamp

/**
 * Created by i_komarov on 28.06.17.
 */

object DateTimeProvider {

    val currentZone: DateTimeZone by lazy {
        DateTimeZone.forOffsetMillis(DateTimeZone.forID("Europe/Moscow").getOffset(DateTime.now()))
    }

    val inputDateTimeFormatter: DateTimeFormatter by lazy {
        DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(DateTimeZone.UTC)
    }

    val outputDateTimeFormatter: DateTimeFormatter by lazy {
        DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(currentZone)
    }
}

fun currentDateTime(): DateTime = DateTime.now(currentZone)

fun dateTimeSerialize(dateTime: DateTime): String = outputDateTimeFormatter.print(dateTime)

fun dateTimeDeserialize(dateTime: String): DateTime = inputDateTimeFormatter.parseDateTime(dateTime)

fun dateTimeSerializeSql(dateTime: DateTime): Timestamp = Timestamp(dateTime.millis)

fun dateTimeDeserializeSql(dateTime: Timestamp): DateTime = DateTime(dateTime, DateTimeZone.UTC)