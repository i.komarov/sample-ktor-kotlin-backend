package net.styleru.ikomarov.diabetics.extentions

import net.styleru.ikomarov.diabetics.dbo.base.DeleteOperationResult
import net.styleru.ikomarov.diabetics.exceptions.RecordNotExistsException

/**
 * Created by i_komarov on 01.07.17.
 */

fun <T> parseReadOperationResult(affectedTable: String, recordId: Long, record: T?): T = when {
    record == null -> throw RecordNotExistsException(affectedTable, listOf(recordId))
    else -> record
}

fun parseDeleteQueryResult(affectedTable: String, recordsIds: List<Long>, deletedRows: Int): DeleteOperationResult = when {
    deletedRows != recordsIds.size -> throw RecordNotExistsException(affectedTable, recordsIds)
    else -> DeleteOperationResult(deletedRows, listOf(affectedTable))
}
