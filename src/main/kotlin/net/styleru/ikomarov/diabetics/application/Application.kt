package net.styleru.ikomarov.diabetics.application

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import net.styleru.ikomarov.diabetics.controllers.categories.categoriesApi
import net.styleru.ikomarov.diabetics.dbo.cateogories.CategoryDBO
import io.ebean.EbeanServer
import io.ebean.EbeanServerFactory
import io.ebean.config.ServerConfig
import net.styleru.ikomarov.diabetics.controllers.authorization.authorizationApi
import net.styleru.ikomarov.diabetics.controllers.authorization.authorizationApiScripts
import net.styleru.ikomarov.diabetics.controllers.errors.HttpErrorProcessor
import net.styleru.ikomarov.diabetics.controllers.registration.registrationApi
import net.styleru.ikomarov.diabetics.controllers.topics.topicsApi
import net.styleru.ikomarov.diabetics.controllers.users.userImagesApi
import net.styleru.ikomarov.diabetics.dao.categories.CategoriesDAO
import net.styleru.ikomarov.diabetics.dao.topics.TopicsDAO
import net.styleru.ikomarov.diabetics.dbo.topics.TopicDBO
import net.styleru.ikomarov.diabetics.mapping.dto.DateTimeSerializer
import net.styleru.ikomarov.diabetics.repository.categories.CategoriesRepository
import net.styleru.ikomarov.diabetics.repository.resources.ResourcesRepository
import net.styleru.ikomarov.diabetics.repository.topics.TopicsRepository
import net.styleru.ikomarov.diabetics.view.authorization.authorizationView
import net.styleru.ikomarov.diabetics.view.registration.registrationView
import org.jetbrains.ktor.application.call
import org.jetbrains.ktor.application.install
import org.jetbrains.ktor.features.*
import org.jetbrains.ktor.host.embeddedServer
import org.jetbrains.ktor.http.HttpStatusCode
import org.jetbrains.ktor.jetty.Jetty
import org.jetbrains.ktor.jetty.JettyApplicationHost
import org.jetbrains.ktor.locations.Locations
import org.jetbrains.ktor.logging.CallLogging
import org.jetbrains.ktor.routing.routing
import org.joda.time.DateTime

/**
 * Created by i_komarov on 28.06.17.
 */

fun main(args: Array<String>) {
    val sqlServer = initializeSqlConnection()
    val gson = initializeJsonParser()

    val errorProcessorFactory = HttpErrorProcessor.Factory(gson = gson)

    val categoriesErrorProcessor = errorProcessorFactory.create("/api/categories/{id?}")
    val topicsErrorProcessor = errorProcessorFactory.create("/api/categories/{category_id}/topics/{id?}")
    val imagesErrorProcessor = errorProcessorFactory.create("/api/users/{id?}/image")
    val authorizationErrorProcessor = errorProcessorFactory.create("/api/authorization")
    val authorizationScriptsErrorProcessor = errorProcessorFactory.create("/api/authorization/scripts/{name}")
    val registrationErrorProcessor = errorProcessorFactory.create("/api/registration")

    val categoriesRepository = CategoriesRepository(database = CategoriesDAO(sqlServer))
    val topicsRepository = TopicsRepository(database = TopicsDAO(sqlServer))
    val resourcesRepository = ResourcesRepository(classLoader = Thread.currentThread().contextClassLoader)

    val webServer: JettyApplicationHost = embeddedServer(Jetty, 5000) {
        install(DefaultHeaders)
        install(CallLogging)
        install(ConditionalHeaders)
        install(PartialContentSupport)
        install(Compression)
        install(Locations)
        install(StatusPages) {
            exception<NotImplementedError> { call.respond(HttpStatusCode.NotImplemented) }
        }

        routing {
            categoriesApi(
                    repository = categoriesRepository,
                    gson = gson,
                    errorProcessor = categoriesErrorProcessor
            )
            topicsApi(
                    repository = topicsRepository,
                    gson = gson,
                    errorProcessor = topicsErrorProcessor
            )
            userImagesApi(
                    gson = gson,
                    errorProcessor = imagesErrorProcessor
            )
            authorizationApi(
                    gson = gson,
                    errorProcessor = authorizationErrorProcessor
            )
            authorizationView(
                    repository = resourcesRepository
            )
            authorizationApiScripts(
                    repository = resourcesRepository,
                    gson = gson,
                    errorProcessor = authorizationScriptsErrorProcessor
            )
            registrationApi(
                    gson = gson,
                    errorProcessor = registrationErrorProcessor
            )
            registrationView(

            )
        }
    }.apply {
        start(wait = true)
    }
}

fun initializeSqlConnection(): EbeanServer = EbeanServerFactory.create(
        ServerConfig().apply {
            loadFromProperties()
            addClass(CategoryDBO::class.java)
            addClass(TopicDBO::class.java)
        }
)

fun initializeJsonParser(): Gson = GsonBuilder()
        .registerTypeAdapter(DateTime::class.java, DateTimeSerializer())
        .create()
