package net.styleru.ikomarov.diabetics.dbo.base

/**
 * Created by i_komarov on 28.06.17.
 */
data class DeleteOperationResult(val deletedRows: Int, val affectedTables: List<String>)