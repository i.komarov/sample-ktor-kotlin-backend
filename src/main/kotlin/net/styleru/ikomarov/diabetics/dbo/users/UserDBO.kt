package net.styleru.ikomarov.diabetics.dbo.users

import net.styleru.ikomarov.diabetics.contracts.users.UserRole
import net.styleru.ikomarov.diabetics.dbo.base.MutableDBO
import java.sql.Timestamp
import javax.persistence.*

/**
 * Created by i_komarov on 03.07.17.
 */

@Entity
@Table(name = "users")
class UserDBO(
    id: Long?,
    createdTime: Timestamp?,
    lastModifiedTime: Timestamp?,
    @Column(name = "first_name")
    val firstName: String,
    @Column(name = "last_name")
    val lastName: String,
    @Column(name = "image_url")
    val imageUrl: String,
    @Column(name = "phone")
    val phone: String,
    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    val role: UserRole
): MutableDBO(id, createdTime, lastModifiedTime)