package net.styleru.ikomarov.diabetics.dbo.base

import net.styleru.ikomarov.diabetics.extentions.currentDateTime
import net.styleru.ikomarov.diabetics.extentions.dateTimeSerializeSql
import java.sql.Timestamp
import javax.persistence.*

/**
 * Created by i_komarov on 28.06.17.
 */

@MappedSuperclass
open class MutableDBO protected constructor(
        id: Long? = null,
        @Temporal(TemporalType.TIMESTAMP)
        @Column(name = "created_time", columnDefinition = "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP", nullable = false, insertable = false, updatable = false)
        var createdTime: Timestamp? = null,
        @Temporal(TemporalType.TIMESTAMP)
        @Column(name = "last_modified_time", columnDefinition = "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT TIMESTAMP", nullable = false, insertable = false, updatable = false)
        var lastModifiedTime: Timestamp? = null
): ImmutableDBO(id) {

    @PrePersist
    fun beforeInsert() = dateTimeSerializeSql(currentDateTime()).let {
        createdTime = Timestamp(it.time)
        lastModifiedTime = Timestamp(it.time)
    }

    @PreUpdate
    fun beforeUpdate() = dateTimeSerializeSql(currentDateTime()).let {
        lastModifiedTime = Timestamp(it.time)
    }
}