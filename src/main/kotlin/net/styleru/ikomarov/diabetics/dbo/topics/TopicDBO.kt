package net.styleru.ikomarov.diabetics.dbo.topics

import net.styleru.ikomarov.diabetics.dbo.base.MutableDBO
import net.styleru.ikomarov.diabetics.dbo.cateogories.CategoryDBO
import java.sql.Date
import java.sql.Timestamp
import javax.persistence.*

/**
 * Created by i_komarov on 02.07.17.
 */

@Entity
@Table(name = "topicsApi")
class TopicDBO(id: Long?,
               createdTime: Timestamp?,
               lastModifiedTime: Timestamp?,
               @Column(name = "category_id")
               private val categoryId: Long?,
               @Column(name = "title")
               val title: String,
               @Column(name = "content")
               val content: String
): MutableDBO(id, createdTime, lastModifiedTime) {

    @JoinColumn(name = "category_id", referencedColumnName = "_id")
    @ManyToOne(optional = false, cascade = arrayOf(CascadeType.REMOVE))
    lateinit var category: CategoryDBO
}