package net.styleru.ikomarov.diabetics.dbo.base

/**
 * Created by i_komarov on 28.06.17.
 */
data class CreateOperationResult(val insertedRows: Int, val updatedRows: Int, val affectedTables: List<String>)