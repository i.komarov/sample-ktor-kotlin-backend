package net.styleru.ikomarov.diabetics.dbo.base

import javax.persistence.*

/**
 * Created by i_komarov on 28.06.17.
 */

@MappedSuperclass
open class ImmutableDBO protected constructor(
        @Id
        @Column(name = "_id")
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long? = null
)