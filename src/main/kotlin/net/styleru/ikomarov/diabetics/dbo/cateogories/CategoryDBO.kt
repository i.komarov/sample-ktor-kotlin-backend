package net.styleru.ikomarov.diabetics.dbo.cateogories

import net.styleru.ikomarov.diabetics.dbo.base.MutableDBO
import net.styleru.ikomarov.diabetics.dbo.topics.TopicDBO
import java.sql.Date
import java.sql.Timestamp
import javax.persistence.*

/**
 * Created by i_komarov on 28.06.17.
 */

@Entity
@Table(name = "categoriesApi")
class CategoryDBO constructor(
        id: Long? = null,
        createdTime: Timestamp? = null,
        lastModifiedTime: Timestamp? = null,
        @Column(name = "title")
        val title: String
): MutableDBO(id, createdTime, lastModifiedTime) {

    @JoinColumn(name = "category_id", referencedColumnName = "_id")
    @OneToMany(cascade = arrayOf(CascadeType.REMOVE))
    lateinit var topics: List<TopicDBO>
}