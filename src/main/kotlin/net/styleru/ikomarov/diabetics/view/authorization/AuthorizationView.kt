package net.styleru.ikomarov.diabetics.view.authorization

import kotlinx.html.*
import net.styleru.ikomarov.diabetics.repository.resources.IResourcesRepository
import org.jetbrains.ktor.application.call
import org.jetbrains.ktor.content.LocalFileContent
import org.jetbrains.ktor.html.respondHtml
import org.jetbrains.ktor.http.ContentType
import org.jetbrains.ktor.http.HttpStatusCode
import org.jetbrains.ktor.response.contentType
import org.jetbrains.ktor.routing.Route
import org.jetbrains.ktor.routing.get
import java.io.File

/**
 * Created by i_komarov on 03.07.17.
 */

private object AuthorizationPage {

    val style: File by lazy { File(Thread.currentThread().getContextClassLoader().getResource("css/authorization.css").file) }
}

fun Route.authorizationView(repository: IResourcesRepository) {
    get("/authorization/styles.css") {
        with(call) {
            response.status(HttpStatusCode.OK)
            response.contentType(ContentType.Text.CSS)
            respond(LocalFileContent(AuthorizationPage.style))
        }
    }
    get("/authorization") {
        with(call) {
            respondHtml {
                head {
                    meta { charset = "UTF-8" }
                    title { + "Авторизация" }
                    link(rel = "stylesheet", href = "http://localhost:5000/authorization/styles.css", type = "text/css")
                    script(type = ScriptType.textJavaScript, src = "http://localhost:5000/api/authorization/scripts/script_authorization_credentials.js")
                }
                body {
                    div {
                        id = "login"
                        form(action = "javascript:void(0);", method = FormMethod.get) {
                            fieldSet(classes = "clearfix") {
                                p {
                                    span(classes = "fontawesome-user")
                                    input(type = InputType.text) {
                                        id = "input_login"
                                        value = "Логин"
                                        onBlur = "if(this.value == '') this.value = 'Логин'"
                                        onFocus= "if(this.value == 'Логин') this.value = ''"
                                        required = true
                                    }
                                }
                                p {
                                    span(classes = "fontawesome-lock")
                                    input(type = InputType.password) {
                                        id = "input_password"
                                        value = "Пароль"
                                        onBlur = "if(this.value == '') this.value = 'Пароль'"
                                        onFocus= "if(this.value == 'Пароль') this.value = ''"
                                        required = true
                                    }
                                }
                                p {
                                    input {
                                        id = "button_authorize"
                                        type = InputType.submit
                                        value = "ВОЙТИ"
                                        onClick = "sendCredentials(document.getElementById(\"input_login\").value, document.getElementById(\"input_password\").value);"
                                    }
                                }
                            }
                        }

                        p {
                            + "Нет аккаунта?  "
                            a(href = "http://localhost:5000/registration") {
                                + "Регистрация"
                            }
                            span(classes = "fontawesome-arrow-right") {

                            }
                        }
                    }
                }
            }
        }
    }
}