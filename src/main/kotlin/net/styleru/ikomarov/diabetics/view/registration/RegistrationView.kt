package net.styleru.ikomarov.diabetics.view.registration

import kotlinx.html.*
import org.jetbrains.ktor.application.call
import org.jetbrains.ktor.content.LocalFileContent
import org.jetbrains.ktor.html.respondHtml
import org.jetbrains.ktor.http.ContentType
import org.jetbrains.ktor.http.HttpStatusCode
import org.jetbrains.ktor.response.contentType
import org.jetbrains.ktor.routing.Route
import org.jetbrains.ktor.routing.get

/**
 * Created by i_komarov on 03.07.17.
 */

fun Route.registrationView() {
    get("/registration") {
        with(call) {
            response.status(HttpStatusCode.NotImplemented)
            response.contentType(ContentType.Text.Html)
            respondHtml(status = HttpStatusCode.NotImplemented) {
                head {
                    meta { charset = "UTF-8" }
                    title { + "Регистрация" }
                }
                body {
                    div {
                        h2 {
                            b {
                                + "501: Not implemented"
                            }
                        }
                    }
                }
            }
        }
    }
}