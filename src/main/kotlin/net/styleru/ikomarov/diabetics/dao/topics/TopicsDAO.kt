package net.styleru.ikomarov.diabetics.dao.topics

import io.ebean.EbeanServer
import io.ebean.TxType
import io.ebean.annotation.Transactional
import net.styleru.ikomarov.diabetics.dao.base.ICrudDAO
import net.styleru.ikomarov.diabetics.dbo.base.CreateOperationResult
import net.styleru.ikomarov.diabetics.dbo.base.DeleteOperationResult
import net.styleru.ikomarov.diabetics.dbo.topics.TopicDBO
import net.styleru.ikomarov.diabetics.dbo.topics.query.QTopicDBO
import net.styleru.ikomarov.diabetics.extentions.dateTimeSerializeSql
import net.styleru.ikomarov.diabetics.extentions.parseDeleteQueryResult
import net.styleru.ikomarov.diabetics.extentions.parseReadOperationResult
import org.joda.time.DateTime

/**
 * Created by i_komarov on 02.07.17.
 */
class TopicsDAO(private val sql: EbeanServer): ICrudDAO<TopicDBO> {

    @Transactional(type = TxType.REQUIRES_NEW)
    override fun create(entity: TopicDBO): CreateOperationResult = sql.insert(entity)
            .let { CreateOperationResult(1, 0, listOf("topicsApi")) }

    @Transactional(type = TxType.REQUIRES_NEW)
    override fun create(entities: List<TopicDBO>): CreateOperationResult = sql.insert(entities)
            .let { CreateOperationResult(entities.size, 0, listOf("topicsApi")) }

    @Transactional(type = TxType.REQUIRED)
    override fun read(id: Long): TopicDBO = QTopicDBO()
            .setId(id)
            .findOne()
            .let { parseReadOperationResult("topicsApi", id, it) }

    @Transactional(type = TxType.REQUIRED)
    override fun readList(parentId: Long): List<TopicDBO> = QTopicDBO()
            .where()
            .category.id.eq(parentId)
            .orderBy().lastModifiedTime.desc()
            .findList()

    @Transactional(type = TxType.REQUIRED)
    override fun readList(parentId: Long, lastModified: DateTime): List<TopicDBO> = QTopicDBO()
            .where()
            .lastModifiedTime.gt(dateTimeSerializeSql(lastModified))
            .and()
            .category.id.eq(parentId)
            .orderBy().lastModifiedTime.desc()
            .findList()

    @Transactional(type = TxType.REQUIRED)
    override fun readList(parentId: Long, offset: Int, limit: Int): List<TopicDBO>  = QTopicDBO()
            .where()
            .category.id.eq(parentId)
            .setFirstRow(offset).setMaxRows(limit)
            .orderBy().lastModifiedTime.desc()
            .findList()

    @Transactional(type = TxType.REQUIRES_NEW)
    override fun update(entity: TopicDBO): CreateOperationResult = sql.update(entity)
            .let { CreateOperationResult(0, 1, listOf("topicsApi")) }

    @Transactional(type = TxType.REQUIRES_NEW)
    override fun update(entities: List<TopicDBO>): CreateOperationResult = sql.updateAll(entities)
            .let { CreateOperationResult(0, entities.size, listOf("topicsApi")) }

    @Transactional(type = TxType.REQUIRES_NEW)
    override fun delete(ids: List<Long>): DeleteOperationResult = QTopicDBO()
            .setIdIn(ids)
            .delete()
            .let { parseDeleteQueryResult("topicsApi", ids, it) }

    @Transactional(type = TxType.REQUIRES_NEW)
    override fun delete(id: Long): DeleteOperationResult = QTopicDBO()
            .setId(id)
            .delete()
            .let { parseDeleteQueryResult("topicsApi", listOf(id), it) }
}