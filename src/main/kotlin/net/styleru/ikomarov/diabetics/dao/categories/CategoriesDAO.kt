package net.styleru.ikomarov.diabetics.dao.categories

import net.styleru.ikomarov.diabetics.dbo.base.CreateOperationResult
import net.styleru.ikomarov.diabetics.dbo.base.DeleteOperationResult
import net.styleru.ikomarov.diabetics.dbo.cateogories.CategoryDBO
import io.ebean.EbeanServer
import io.ebean.TxType
import io.ebean.annotation.Transactional
import net.styleru.ikomarov.diabetics.dao.base.ICrudDAO
import net.styleru.ikomarov.diabetics.dbo.cateogories.query.QCategoryDBO
import net.styleru.ikomarov.diabetics.extentions.dateTimeSerializeSql
import net.styleru.ikomarov.diabetics.extentions.parseDeleteQueryResult
import net.styleru.ikomarov.diabetics.extentions.parseReadOperationResult
import org.joda.time.DateTime

/**
 * Created by i_komarov on 28.06.17.
 */
class CategoriesDAO constructor(private val sql: EbeanServer): ICrudDAO<CategoryDBO> {

    @Transactional(type = TxType.REQUIRES_NEW)
    override fun create(entity: CategoryDBO): CreateOperationResult = sql.insert(entity)
            .let { CreateOperationResult(1, 0, listOf("categoriesApi")) }

    @Transactional(type = TxType.REQUIRES_NEW)
    override fun create(entities: List<CategoryDBO>): CreateOperationResult = sql.insertAll(entities)
            .let { CreateOperationResult(entities.size, 0, listOf("categoriesApi")) }

    @Transactional(type = TxType.REQUIRED)
    override fun read(id: Long): CategoryDBO = QCategoryDBO()
            .setId(id)
            .findOne()
            .let { parseReadOperationResult("categoriesApi", id, it) }

    @Transactional(type = TxType.REQUIRED)
    override fun readList(parentId: Long): List<CategoryDBO> = QCategoryDBO()
            .orderBy().lastModifiedTime.desc()
            .findList()

    @Transactional(type = TxType.REQUIRED)
    override fun readList(parentId: Long, lastModified: DateTime): List<CategoryDBO> = QCategoryDBO()
            .where()
            .lastModifiedTime.gt(dateTimeSerializeSql(lastModified))
            .orderBy().lastModifiedTime.desc()
            .findList()

    @Transactional(type = TxType.REQUIRED)
    override fun readList(parentId: Long, offset: Int, limit: Int): List<CategoryDBO> = QCategoryDBO()
            .setFirstRow(offset).setMaxRows(limit)
            .orderBy().lastModifiedTime.desc()
            .findList()

    @Transactional(type = TxType.REQUIRES_NEW)
    override fun update(entity: CategoryDBO): CreateOperationResult = sql.update(entity)
            .let { CreateOperationResult(0, 1, listOf("categoriesApi")) }

    @Transactional(type = TxType.REQUIRES_NEW)
    override fun update(entities: List<CategoryDBO>): CreateOperationResult = sql.updateAll(entities)
            .let { CreateOperationResult(0, entities.size, listOf("categoriesApi")) }

    @Transactional(type = TxType.REQUIRES_NEW)
    override fun delete(id: Long): DeleteOperationResult = QCategoryDBO()
            .setId(id)
            .delete()
            .let { parseDeleteQueryResult("categoriesApi", listOf(id), it) }

    @Transactional(type = TxType.REQUIRES_NEW)
    override fun delete(ids: List<Long>): DeleteOperationResult = QCategoryDBO()
            .setIdIn(ids)
            .delete()
            .let { parseDeleteQueryResult("categoriesApi", ids, it) }
}