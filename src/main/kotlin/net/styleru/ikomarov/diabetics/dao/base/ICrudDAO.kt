package net.styleru.ikomarov.diabetics.dao.base

import net.styleru.ikomarov.diabetics.dbo.base.CreateOperationResult
import net.styleru.ikomarov.diabetics.dbo.base.DeleteOperationResult
import org.joda.time.DateTime

/**
 * Created by i_komarov on 28.06.17.
 */
interface ICrudDAO<T> {

    fun create(entity: T): CreateOperationResult

    fun create(entities: List<T>): CreateOperationResult

    fun read(id: Long): T

    fun readList(parentId: Long = 0L): List<T>

    fun readList(parentId: Long = 0L, lastModified: DateTime): List<T>

    fun readList(parentId: Long = 0L, offset: Int, limit: Int): List<T>

    fun update(entity: T): CreateOperationResult

    fun update(entities: List<T>): CreateOperationResult

    fun delete(ids: List<Long>): DeleteOperationResult

    fun delete(id: Long): DeleteOperationResult
}