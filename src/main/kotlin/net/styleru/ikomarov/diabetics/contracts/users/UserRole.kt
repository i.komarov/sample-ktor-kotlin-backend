package net.styleru.ikomarov.diabetics.contracts.users

/**
 * Created by i_komarov on 03.07.17.
 */
enum class UserRole(val role: String) {
    EXPERT("expert"),
    USER("user"),
    GUEST("guest")
}