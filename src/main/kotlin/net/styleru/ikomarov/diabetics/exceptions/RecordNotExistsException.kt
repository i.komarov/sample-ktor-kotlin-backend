package net.styleru.ikomarov.diabetics.exceptions

/**
 * Created by i_komarov on 01.07.17.
 */

class RecordNotExistsException(val table: String, val ids: List<Long>): IllegalStateException("Record with given ids: \"$ids\" does not exist in required context: \"$table\"")