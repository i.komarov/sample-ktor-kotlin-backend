package net.styleru.ikomarov.diabetics.exceptions

/**
 * Created by i_komarov on 01.07.17.
 */

class RecordsNotExistException(val affectedTable: String, val ids: List<Long>): IllegalStateException("Some or all of these records were not found: \"$ids\" in table: \"$affectedTable\"")