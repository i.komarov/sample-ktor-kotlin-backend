package net.styleru.ikomarov.diabetics.dto.topics

import com.google.gson.annotations.SerializedName
import net.styleru.ikomarov.diabetics.dto.base.MutableDTO
import org.joda.time.DateTime

/**
 * Created by i_komarov on 02.07.17.
 */
class TopicDTO(id: Long,
               createdTime: DateTime,
               lastModifiedTime: DateTime,
               @SerializedName("category_id")
               val categoryId: Long? = null,
               @SerializedName("title")
               val title: String,
               @SerializedName("content")
               val content: String,
               @SerializedName("comments_count")
               val commentsCount: Int = 0
): MutableDTO(id, createdTime, lastModifiedTime)