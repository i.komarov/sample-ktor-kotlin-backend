package net.styleru.ikomarov.diabetics.dto.authorization

import com.google.gson.annotations.SerializedName
import net.styleru.ikomarov.diabetics.dto.base.ImmutableDTO

/**
 * Created by i_komarov on 04.07.17.
 */
class CredentialsDTO(
        id: Long = 0L,
        @SerializedName("login")
        val login: String,
        @SerializedName("password")
        val password: String
) : ImmutableDTO(id)