package net.styleru.ikomarov.diabetics.dto.response

import com.google.gson.annotations.SerializedName
import net.styleru.ikomarov.diabetics.dto.base.ImmutableDTO
import net.styleru.ikomarov.diabetics.dto.error.ErrorDTO

/**
 * Created by i_komarov on 28.06.17.
 */
class StandardResponse<Body> private constructor(@SerializedName("body") val body: Body?, @SerializedName("error") val error: ErrorDTO?) {

    companion object Factory {

        fun <Body: ImmutableDTO> success(body: Body): StandardResponse<Body> = StandardResponse(body, null)

        fun <Body: List<ImmutableDTO>> success(body: Body): StandardResponse<Body> = StandardResponse(body, null)

        fun <Body> failure(error: ErrorDTO): StandardResponse<Body> = StandardResponse(null, error)
    }
}