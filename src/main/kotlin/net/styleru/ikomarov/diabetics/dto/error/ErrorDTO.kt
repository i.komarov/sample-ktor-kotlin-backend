package net.styleru.ikomarov.diabetics.dto.error

import com.google.gson.annotations.SerializedName
import net.styleru.ikomarov.diabetics.dto.base.ImmutableDTO

/**
 * Created by i_komarov on 28.06.17.
 */
class ErrorDTO constructor(id: Long = -1L, @SerializedName("message") val message: String, @SerializedName("api_url") val apiUrl: String): ImmutableDTO(id)