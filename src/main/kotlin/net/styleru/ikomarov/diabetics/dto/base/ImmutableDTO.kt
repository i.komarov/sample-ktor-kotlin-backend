package net.styleru.ikomarov.diabetics.dto.base

import com.google.gson.annotations.SerializedName

/**
 * Created by i_komarov on 28.06.17.
 */
open class ImmutableDTO(
        @SerializedName("id")
        val id: Long
)