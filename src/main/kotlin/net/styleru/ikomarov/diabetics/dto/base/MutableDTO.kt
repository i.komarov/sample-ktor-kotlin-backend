package net.styleru.ikomarov.diabetics.dto.base

import com.google.gson.annotations.SerializedName
import org.joda.time.DateTime;

/**
 * Created by i_komarov on 28.06.17.
 */
open class MutableDTO constructor(
        id: Long,
        @SerializedName("created_time")
        val createdTime: DateTime,
        @SerializedName("last_modified_time")
        val lastModifiedTime: DateTime
): ImmutableDTO(id)