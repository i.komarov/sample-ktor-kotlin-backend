package net.styleru.ikomarov.diabetics.dto.categories

import com.google.gson.annotations.SerializedName
import net.styleru.ikomarov.diabetics.dto.base.MutableDTO
import org.joda.time.DateTime;

/**
 * Created by i_komarov on 28.06.17.
 */
class CategoryDTO constructor(
        id: Long,
        createdTime: DateTime,
        lastModifiedTime: DateTime,
        @SerializedName("title")
        val title: String,
        @SerializedName("topics_count")
        val topicsCount: Int
): MutableDTO(id, createdTime, lastModifiedTime)