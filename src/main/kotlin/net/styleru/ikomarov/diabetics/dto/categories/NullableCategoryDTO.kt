package net.styleru.ikomarov.diabetics.dto.categories

import com.google.gson.annotations.SerializedName
import org.joda.time.DateTime

/**
 * Created by i_komarov on 03.07.17.
 */

class NullableCategoryDTO constructor(
        @SerializedName("id")
        val id: Long? = null,
        @SerializedName("created_time")
        val createdTime: DateTime? = null,
        @SerializedName("last_modified_time")
        val lastModifiedTime: DateTime? = null,
        @SerializedName("title")
        val title: String? = null,
        @SerializedName("topics_count")
        val topicsCount: Int? = null
)